const { Router } = require("express");
const router = Router();
const { createUser, renderLogin, renderIndex, renderArticle, renderEditor } = require("../controllers/index.controller");


router.get('/', renderIndex);

router.get('/input-user', renderLogin);

router.get('/article/:id', renderArticle);

router.get('/editor', renderEditor);

router.post('/create-user', createUser);

module.exports = router;
