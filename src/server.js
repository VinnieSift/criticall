//inicializaciones
const express = require('express');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const path = require('path');


//Global Variables
const app = express();

//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));


//configuraciones
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, "views"));
app.engine('.hbs', exphbs({
    defaultLayout:'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}));

app.set('view engine', '.hbs');

//Routes
app.use(require('./routes/index.routes'));


//statics files

app.use(express.static(path.join(__dirname, "public")));


module.exports = app;