function carrouselBigCenterImage() {
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
    let ret=0;
    if(vw < 1200){
        ret=(1200-vw)/2;
    }
    //document.getElementsByClassName("image-1").style.transform = "translateX(-"+ret.toString()+"px)"; 
    let images = document.getElementsByClassName("image-main");
    let imagesSize = document.getElementsByClassName("image-main").length;
    let cont=0;
    for(cont=0;cont<imagesSize;cont++){
        images[cont].style.transform = "translateX(-"+ret.toString()+"px)";
    }
}